const vars = {
  colors: {
    primary: '#eb5c2e',
    key: '#000',
    white: '#fff',
  },
  header: {
    height: 70,
  },
  footer: {
    height: 100,
  },
  media: {
    xxsmall: '@media (min-width: 320px)',
    xsmall: '@media (min-width: 400px)',
    small: '@media (min-width: 600px)',
    medium: '@media (min-width: 1024px)',
    large: '@media (min-width: 1280px)',
    xlarge: '@media (min-width: 1440px)',
    xxlarge: '@media (min-width: 1920px)',
  },
}

export default vars
