import {createGlobalStyle} from 'styled-components'
import vars from './vars'
import dcbwoff from '../assets/fonts/DINCondensed-Bold.woff'
import dcbwoff2 from '../assets/fonts/DINCondensed-Bold.woff2'
import dprwoff from '../assets/fonts/DINPro-Regular.woff'
import dprwoff2 from '../assets/fonts/DINPro-Regular.woff2'
import dpbwoff2 from '../assets/fonts/DINPro-Bold.woff2'
import dpbwoff from '../assets/fonts/DINPro-Bold.woff'
import dpmwoff2 from '../assets/fonts/DINPro-Medium.woff2'
import dpmwoff from '../assets/fonts/DINPro-Medium.woff'
import rcondeot from '../assets/fonts/RobotoCondensed-Regular.eot'
import rcondsvg from '../assets/fonts/RobotoCondensed-Regular.svg'
import rcondttf from '../assets/fonts/RobotoCondensed-Regular.ttf'
import rcondwoff from '../assets/fonts/RobotoCondensed-Regular.woff'
import rcondwoff2 from '../assets/fonts/RobotoCondensed-Regular.woff2'

const global = createGlobalStyle`
  * {
    box-sizing: border-box;
    outline: none;
    -webkit-tap-highlight-color: transparent;
    &::selection {
      background: ${vars.colors.primary};
      color: ${vars.colors.white};
    }
  }
  html, body, #root {
    height: 100%;
  }
  html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }
  strong {
    font-weight: bold;
  }
  article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
    display: block;
  }
  ol, ul {
    list-style: none;
  }
  blockquote, q {
    quotes: none;
  }
  table {
    border-collapse: collapse;
    border-spacing: 0;
  }
  svg {
    display: inline-block;
    vertical-align: top;
  }
  @font-face {
    font-family: 'DINPro';
    font-style: normal;
    font-weight: 400;
    src: local('DINPro'), local('DINPro-Regular'),
         url('${dprwoff2}') format('woff2'),
         url('${dprwoff}') format('woff');
  }
  @font-face {
    font-family: 'DINPro';
    font-style: normal;
    font-weight: 500;
    src: local('DINPro'), local('DINPro-Medium'),
         url('${dpmwoff2}') format('woff2'),
         url('${dpmwoff}') format('woff');
  }
  @font-face {
    font-family: 'DINPro';
    font-style: normal;
    font-weight: 700;
    src: local('DINPro'), local('DINPro-Bold'),
         url('${dpbwoff2}') format('woff2'),
         url('${dpbwoff}') format('woff');
  }
  @font-face {
    font-family: 'DINCond';
    font-style: normal;
    font-weight: bold;
    src: local('DINCond'), local('DINCond'),
         url('${dcbwoff2}') format('woff2'),
         url('${dcbwoff}') format('woff');
  }
  @font-face {
    font-family: 'Roboto Condensed';
    src: url('${rcondeot}');
    src: url('${rcondeot}?#iefix') format('embedded-opentype'),
        url('${rcondwoff2}') format('woff2'),
        url('${rcondwoff}') format('woff'),
        url('${rcondttf}') format('truetype'),
        url('${rcondsvg}#RobotoCondensed-Regular') format('svg');
    font-weight: normal;
    font-style: normal;
  }
  .mapboxgl-popup {
    z-index: 95;
  }
  .mapboxgl-popup-tip {
    display: none!important;
  }
  .mapboxgl-popup-content {
    max-width: 180px;
    displa; block!important;
    padding: 0!important;
    box-shadow: none!important;
    background: none!important;
    z-index: 100;
  }
`

export default global
