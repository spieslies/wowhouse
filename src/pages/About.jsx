import React from 'react'
import {observer} from 'mobx-react'
import styled from 'styled-components'
import vars from '../styles/vars'
import stores from '../stores'
import CloseCrossIcon from '../components/icons/CloseCrossIcon'

const Self = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: ${vars.colors.key};
  color: ${vars.colors.white};
  width: 100%;
  padding: 60px;
  overflow-y: scroll;
  z-index: 1000;
  ${vars.media.small} {
    padding: 35px 100px 35px 60px;
    width: calc(30% + 16px);
    position: absolute;
  }
  ${vars.media.xlarge} {
    width: calc(100% - 875px);  
  }
`
const Title = styled.h1`
  color: ${vars.colors.primary};
  margin-bottom: 35px;
  font-size: 25px;
  ${vars.media.medium} {
    font-size: 20px;
  }
  ${vars.media.large} {
    font-size: 35px;
  }
`
const Paragraph = styled.p`
  &:not(:last-child) {
    margin-bottom: 25px; 
  }
`
const List = styled.div`
  margin-top: 50px;
`
const ListTitle = styled.div`
  color: ${vars.colors.primary};
  text-transform: uppercase;
  font-size: 13px;
  margin-bottom: 10px;
`
const Close = styled.div`
  width: 25px;
  height: 25px;
  fill: #fff;
  position: fixed;
  top: 20px;
  right: 15px;
  ${vars.media.small} {
    top: 125px;
    right: 44px;
  }
  ${vars.media.medium} {
    width: 30px;
    height: 30px;
  }
  ${vars.media.large} {
    width: 50px;
    height: 50px;
  }
  &:hover {
    fill: ${vars.colors.primary};
    cursor: pointer;
  }
  
`
const Link = styled.a`
  color: inherit;
  &:hover {
    color: ${vars.colors.primary};
    cursor: pointer;
  }
`
@observer
export default class About extends React.Component {
  render() {
    if (!stores.mainStore.showPopup) {
      return null
    }
    else {
      return (
        <Self>
          <Close onClick={()=>stores.mainStore.togglePopup()}>
            <CloseCrossIcon/>
          </Close>
          <Title>О ПРОЕКТЕ "МОСКВА 08-18"</Title>
          <Paragraph>
            В этом году бюро Wowhaus исполнилось 10 лет. Сегодня понятия
            "общественное пространство" и "комфортная городская среда"
            воспринимаются как нечто само собой разумеющееся. А тогда, в 2008-м,
            когда мы подняли эту тему из небытия, начали общественную дискуссию
            с коллегами и, собственно, взялись эти общественные пространства
            делать - то был глас вопиющего в пустыне. Сейчас в это даже не верится.
          </Paragraph>
          <Paragraph>
            Мы, возможно, и сами не вспомнили бы об этом, но десятилетие стало
            поводом оглянуться назад и осмыслить, какой путь прошло наше бюро,
            прошел наш любимый город и куда теперь идти дальше. Сопоставив
            факты, мы сами удивились, какой интересной была эта дорога и как затейливо
            устроена жизнь.
          </Paragraph>
          <Paragraph>
            Открытия о нашем самом недавнем прошлом подвигли нас на создание для
            собственного и для всеобщего удовольствия и развлечения этой игры
            "08-18". По прошедшему десятилетию можно бродить в любом порядке и
            удивляться, какие разные явления происходили одновременно, как все
            друг на друга влияло и что из этого выходило.
          </Paragraph>
          <Paragraph>
            В этой игре почти нет нашей прямой речи и совсем нет оценки
            происходившего. Это все факты, цитаты, выдержки. Мы не претендуем на
            воссоздание объективной картины мира, это всего лишь субъективный
            взгляд на минувшее десятилетие и попытка осмысления того, как живет
            Город, через призму работы нашей, наших друзей и коллег.
          </Paragraph>
          <Paragraph>
            Нам было интересно. Надеемся, вам тоже будет нескучно. Еще увидимся!
          </Paragraph>
          Ваш WOWHAUS
          <List>
            <ListTitle>Авторский коллектив:</ListTitle>
            Олег Шапиро<br/>
            Дмитрий Ликин<br/>
            Анна Ищенко<br/>
            Дмитрий Чистов (<Link target='_blank' href="http://adworm.ru/">adworm.ru</Link>)<br/>
            Павел Пахомов (<Link target='_blank' href="http://adworm.ru/">adworm.ru</Link>)<br/>
            Дарья Кузнецова (<Link target='_blank' href="http://adworm.ru/">adworm.ru</Link>)<br/>
            Илья Смирнов (<Link target='_blank' href="http://adworm.ru/">adworm.ru</Link>)<br/>
            Людмила Фрост<br/>
            Артем Куликов<br/>
            Аркадий Ненашев<br/>
            Юлия Тарабарина (<Link href="https://archi.ru/">archi.ru</Link>)
          </List>
        </Self>
      )
    }

  }
}
