import React from 'react'
import styled from 'styled-components'

import bg from '../assets/img/404_bg.jpg'
import vars from '../styles/vars'

const Self = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-image:url(${bg});
  background-position: 50%;
  background-size: cover;
  z-index: 100;
`
const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`
const Code = styled.div`
  font-size: 350px;
  color: ${vars.colors.white};
`
const Text = styled.div`
  display: block;
  margin-bottom: 105px;
`
const Inner = styled.span`
  font-size: 30px;
  background-color: ${vars.colors.key};
  color: ${vars.colors.primary};
  text-transform: uppercase;
`

export default class NotFound extends React.Component {
  render() {
    return (
      <Self>
        <Content>
          <Code>404</Code>
          <Text>
            <Inner>Страница<br/>не существует<br/>или удалена</Inner>
          </Text>
        </Content>

      </Self>
    )
  }
}
