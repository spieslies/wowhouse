import React from 'react'
import {observer} from 'mobx-react'

import {ArticlesList, ArticleColumn} from '../components'

import stores from '../stores'
import chunkArray from '../utils/chunkArray'
import ArticlePreview from '../components/article/ArticlePreview'
import Meta from '../utils/Meta'

@observer
export default class Year2008 extends React.Component {

  componentDidMount() {
    stores.mainStore.getArticlesList(this.props.location.pathname.slice(1))
    stores.mainStore.getPins(this.props.location.pathname.slice(1))
    stores.mainStore.setCurrentYear(this.props.location.pathname.slice(1))
  }

  render() {
    const store = stores.mainStore
    return (
      <React.Fragment>
        <Meta title={`${this.props.location.pathname.slice(1)} | МОСКВА 08 - 18`}/>
        <ArticlesList>
          {chunkArray(store.articlesList, 3).map((i, key) => {
            return (
              <ArticleColumn key={key}>
                {i.map((item, key) => {
                  return (
                    <ArticlePreview
                      key={key}
                      to={`/${this.props.location.pathname.slice(1)}/${item.id}`}
                      type={item.type}
                      month={item.month}
                      title={item.previewTitle}
                      author={item.authorName}
                      authorPhoto={item.authorPhoto}
                      onClick={() => stores.mainStore.closePopup()}
                    />
                  )
                })}
              </ArticleColumn>
            )
          })}
        </ArticlesList>
      </React.Fragment>
    )
  }
}
