import React from 'react'
import {observer} from 'mobx-react'

import {ArticlesList, ArticleColumn} from '../components'

import stores from '../stores'
import chunkArray from '../utils/chunkArray'
import ArticlePreview from '../components/article/ArticlePreview'
import Meta from '../utils/Meta'

@observer
export default class Year2018 extends React.Component {

  componentDidMount() {
    stores.mainStore.getArticlesList('2018')
    stores.mainStore.getPins('2018')
    stores.mainStore.setCurrentYear('2018')
  }

  render() {
    const store = stores.mainStore
    const location = this.props.location.pathname.slice(1) === '' ? '2018' : this.props.location.pathname.slice(1)
    return (
      <React.Fragment>
        <Meta title='2018 | МОСКВА 08 - 18'/>
        <ArticlesList>
          {chunkArray(store.articlesList, 3).map((i, key) => {
            return (
              <ArticleColumn key={key}>
                {i.map((item, key) => {
                  return (
                    <ArticlePreview
                      key={key}
                      to={`/${location}/${item.id}`}
                      type={item.type}
                      month={item.month}
                      title={item.previewTitle}
                      author={item.authorName}
                      authorPhoto={item.authorPhoto}
                      onClick={() => stores.mainStore.closePopup()}
                    />
                  )
                })}
              </ArticleColumn>
            )
          })}
        </ArticlesList>
      </React.Fragment>
    )
  }
}
