import React from 'react'
import styled from 'styled-components'
import {observer} from 'mobx-react/index'
import {
  FacebookShareButton,
  TwitterShareButton,
  VKShareButton,
} from 'react-share'

import copy from 'clipboard-copy'

import vars from '../../styles/vars'

import FbIcon from '../icons/FbIcon'
import TwIcon from '../icons/TwIcon'
import VkIcon from '../icons/VkIcon'
import ClIcon from '../icons/ClIcon'

const Self = styled.div`
  display: block;
`
const Title = styled.span`
  display: block;
  margin-bottom: 10px;
  text-transform: uppercase;
  font-size: 13px;
  font-weight: 500;
`
const Content = styled.div`
  display: flex;
`
const Item = styled.div`
  width: 30px;
  height: 30px;
  background-color: ${vars.colors.key};
  display: flex;
  justify-content: center;
  align-items: center;
  & > div {
    width: 100%;
    text-align: center;
  }
  &:not(:last-child) {
    margin-right: 10px;
  }
  &:hover {
    cursor: pointer;
    fill: ${vars.colors.primary};
    background-color: ${vars.colors.primary};
  }
`
@observer
export default class Sharing extends React.Component {

  render() {
    const {url} = this.props
    return (
      <Self>
        <Title>Поделиться:</Title>
        <Content>
          <Item>
            <FacebookShareButton url={url}>
              <FbIcon/>
            </FacebookShareButton>
          </Item>
          <Item>
            <TwitterShareButton url={url}>
              <TwIcon/>
            </TwitterShareButton>
          </Item>
          <Item>
            <VKShareButton url={url}>
              <VkIcon/>
            </VKShareButton>
          </Item>
          <Item onClick={() => copy(url)}>
            <ClIcon/>
          </Item>
        </Content>
      </Self>
    )
  }
}
