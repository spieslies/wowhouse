import React from 'react'
import styled from 'styled-components'
import vars from '../../styles/vars'

const Self = styled.figure`
  width: 100%;
  height: 200px;
  ${vars.media.small} {
    height: 400px;  
  }
  ${vars.media.medium} {
    height: 100%;
  }
`

const Img = styled.img`
  display: block;
  height: 100%;
  width: 100%;
  object-fit: cover;
  filter: grayscale(100%);
`

export default class Image extends React.Component {
  render() {
    const {
      src,
      src2x,
      alt,
    } = this.props
    return (
      <Self>
        <Img
          src={src}
          srcSet={ src2x ? `${src} 1x, ${src2x} 2x` : null}
          alt={alt}
        />
      </Self>
    )
  }
}
