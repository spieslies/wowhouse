import React from 'react'
import styled from 'styled-components'
import {observer} from 'mobx-react'

import stores from '../../stores'
import vars from '../../styles/vars'
import Loading from '../loading/Loading'
import GoToEventsIcon from '../icons/GoToEventsIcon'
import GoToMapIcon from '../icons/GoToMapIcon'

const Self = styled('div')`
  background-color: #fff;
  &::-webkit-scrollbar {
    width: 15px;
  }
  &::-webkit-scrollbar-track {
    background-color: #eee;
  }
  &::-webkit-scrollbar-thumb {
    background: #d4d4d4;
    border-radius: 10px; 
  }
  ${vars.media.small} {
    display: flex;
  }
  ${vars.media.medium} {
    flex-direction: row;
    flex-wrap: wrap;
    align-items: flex-start;
    flex-shrink: 0;
    width: calc(70% - 16px);
    max-width: 875px;
    height: 100%;
    padding: 27px 10px 20px 0;
    overflow-y: auto;  
  }
`
const ShowMap = styled.div`
  position: fixed;
  top: 50vh;
  right: 0;
  display: block;
  width: 50px;
  height: 70px;
  background: rgba(221, 221, 221, .9);
  z-index: 160;
  backface-visibility: hidden;
  ${vars.media.medium} {
    display: none;
  }
`
const Icons = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  padding: 5px;
`
const IconWrapper = styled.div`
  width: 100%;
`

@observer
export default class ArticlesList extends React.Component {
  render() {
    const {children} = this.props
    const store = stores.mainStore
    return (
      <Self>
        {store.loading ? (
          <Loading/>
        ) : (
          children
        )}
        <ShowMap onClick={() => store.toggleMap()}
                 style={store.showMap ? {left: 0} : null}>
          {!store.showMap ? (
            <Icons>
              <IconWrapper>
                <GoToMapIcon/>
              </IconWrapper>
            </Icons>

          ) : (
            <Icons>
              <IconWrapper>
                <GoToEventsIcon/>
              </IconWrapper>
            </Icons>
          )}
        </ShowMap>
      </Self>
    )
  }
}
