import React from 'react'
import styled from 'styled-components'
import vars from '../../styles/vars'

const Self = styled.div`
  width: 100%;
  margin-bottom: 54px;
  ${vars.media.small} {
    width: 33%;
    flex-shrink: 0;
  }
  ${vars.media.medium} {
    margin-bottom: 0;
    &:not(:last-child) {
      padding-right: 10px;
    }
  }
  ${vars.media.xlarge} {
    width: 280px;
  } 
`
export default class ArticleColumn extends React.Component {
  render() {
    const {children} = this.props
    return (
      <Self>{children}</Self>
    )
  }
}
