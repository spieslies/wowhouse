import React from 'react'
import styled from 'styled-components'
import {observer} from 'mobx-react/index'

import ReactMarkdown from 'react-markdown'
import {Link} from 'react-router-dom'

import stores from '../../stores'

import vars from '../../styles/vars'

import CloseCrossIcon from '../icons/CloseCrossIcon'
import Sharing from '../sharing/Sharing'
import articleBlockquote from '../../assets/icons/articleBlockquote.svg'
import Image from './Image'
import ArticlePreview from './ArticlePreview'
import Meta from '../../utils/Meta'

const Self = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  overflow-y: auto;
  background-color: ${vars.colors.white};
  position: unset;
  z-index: 200;
  ${vars.media.medium} {
    position: absolute; 
  }
  
`

const Inner = styled.div`
  display: inline-block;
  position: relative;
  width: 100%;
  height: 100%;
  padding-top: 50px;
  padding-bottom: 80px;
  background-color: ${vars.colors.white};
  z-index: 100;
  ${vars.media.medium} {
    padding-top: 0;
    overflow-y: scroll;
  }
  &::-webkit-scrollbar {
      width: 10px;
    }
    &::-webkit-scrollbar-track {
      background-color: #eee;
    }
    &::-webkit-scrollbar-thumb {
      background: #d4d4d4; 
      border-radius: 10px;
    }
`
const Close = styled(Link)`
  display: block;
  position: fixed;
  top: 140px;
  right: 20px;
  width: 25px;
  height: 25px;
  z-index: 100;
  ${vars.media.medium} {
    position: fixed;
    width: 30px;
    height: 30px;
    top: 144px;
    right: 44px;
  }
  ${vars.media.large} {
    width: 50px;
    height: 50px;
  }
  &:hover {
    cursor: pointer;
    fill: ${vars.colors.primary}
  }
`
const Media = styled.div`
  display: flex;
  flex-direction: column-reverse;
  position: relative;
  box-sizing: border-box;
  margin-bottom: 5px;
  ${vars.media.medium} {
    display: block;
    height: 400px; 
    overflow: hidden;
  }
  ${vars.media.large} {
    height: 483px;
    margin-bottom: 60px;
  }
`
const TitleWrapper = styled.div`
  width: 100%;
  pointer-events: none;
  margin-bottom: -4px;
  margin-left: -3px;
  padding:0 40px;
  ${vars.media.small} {
    width: 70%;
    padding: 0 50px;
  }
  ${vars.media.medium} {
    width: 100%;
    position: absolute;
    bottom: -2px;
    padding: 0;
  }
  &:before {
    display: none;
    content: '';
    position: absolute;
    top: 10px;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: ${vars.colors.primary};
    mix-blend-mode: multiply;
    ${vars.media.medium} {
      display: block;
    }
  }
`
const Title = styled.h1`
  position: relative;
  font-size: 30px;
  text-transform: uppercase;
  line-height: 1;
  z-index: 10;
  color: ${vars.colors.primary};
  ${vars.media.medium} {
    font-size: 45px;
    color: ${vars.colors.white};
    padding-left: 50px;
  }
  ${vars.media.large} {
    width: 870px;
    font-size: 56px;
    padding-left: 190px;
    
  }
`
const Content = styled.div`
  padding: 20px 40px;
  margin-bottom: 60px;
  &:not(:last-child) {
    margin-bottom: 25px;
  }
  ${vars.media.small} {
    padding: 20px 50px;
  }
  ${vars.media.large} {
    padding:0 0 0 190px;
  }
`
const AuthorWrapper = styled.div`
  padding: 0 20px;
  ${vars.media.large} {
    width: 680px;
    padding: 0;
  }
`
const EventAuthor = styled.div`
  display: flex;
  align-items: flex-end;
  margin-bottom: 25px;
  margin-top: 60px;
  ${vars.media.medium} {
   padding-left: 50px;
  }
  ${vars.media.large} {
    padding-left: 70px;
  }
`
const EventAuthorPhoto = styled.img`
  width: 120px;
  height: 91px;
  object-fit: cover;
  object-position: center;
  flex-shrink: 0;
  filter: grayscale(100%);
`
const EventAuthorInfo = styled.div`
  width: 100%;
`
const EventAuthorName = styled.div`
  font-size: 28px;
  line-height: 1;
  text-transform: uppercase;
`
const EventAuthorStatus = styled.div`
  display: inline-block;
  margin-bottom: 5px;
  font-size: 10px;
  line-height: 13px;
  text-transform: uppercase;
  font-weight: 500;
  ${vars.media.medium} {  
    font-size: 11px;
  }
`
const SpeechAuthor = styled.div`
  text-transform: uppercase;
  padding: 20px 40px 0;
  font-size: 12px;
  ${vars.media.small} {
    padding: 0 50px;
  }
  ${vars.media.large} {
    width: 680px;
    font-size: 16px;
    padding: 0 0 0 190px;
      
  }
`
const MarkdownContainer = styled.div`
  display: block;
  font-size: 17px;
  ${vars.media.large} {
    width: 680px;  
  }
  & h1 {
    font-size: 32px;
    font-weight: 500;
  }
  & p:not(:last-child) {
    margin-bottom: 25px;
  }
  & a {
    display: block;
    color: inherit;
    &:hover {
      color: ${vars.colors.primary}
    }
  }
  & iframe {
    width: 100%;
    height: 200px;
    margin: 25px 0;
    filter: grayscale(100%);
    ${vars.media.small} {
      height: 300px;  
    }
    ${vars.media.medium} {
      height: 415px;  
    }
  }
  & img {
    width: 100%;
    height: 200px;
    object-fit: cover;
    filter: grayscale(100%);
    ${vars.media.small} {
      height: 350px;  
    }
    ${vars.media.medium} {
      height: 415px;  
    }
    
  }
  & blockquote {
    position: relative;
    width: 100%;
    padding: 7px 0 0 44px;
    font-size: 20px;
    text-transform: uppercase;
    line-height: 1.2;
    ${vars.media.small} {
      width: 70%;
    }
    ${vars.media.medium} {
      padding: 7px 0 0 0;
      font-size: 27px;
    }
    & span {
      background-color:${vars.colors.primary};
    }
    &:not(:last-child) {
      margin: 25px 0;
    }
    &:before {
      content: '';
      position: absolute;
      top: 6px;
      left: 0;
      display: block;
      width: 44px;
      height: 48px;
      background-image:url(${articleBlockquote});
      background-repeat: no-repeat;
      background-position: 50%;
      background-size: contain;
      ${vars.media.medium} {
        left: -44px;
      }
    }
  }
`
const LeadWrapper = styled.div`
  display: block;
  margin-top: -2px;
  ${vars.media.medium} {
    position: absolute;
    top: 51px;
    left: 0;
    margin: 0;
    z-index: 50;  
  }
`
const Lead = styled.div`
  width: 100%;
  padding: 0 40px 0;
  line-height: 1.1;
  ${vars.media.small} {
    width: 70%;
    padding: 0 50px;
  }
  ${vars.media.medium} {
    width: 430px;
    padding: 0;
    line-height: 1.2;
  }
`
const LeadInner = styled.span`
  text-transform: uppercase;
  font-size: 16px;
  background-color: ${vars.colors.key};
  color: ${vars.colors.white};
  ${vars.media.medium} {
    font-size: 27px;
    letter-spacing: -1px;  
  }
`
const ImageWrapper = styled.div`
  width: 100%;
  height: 100%;
  ${vars.media.large} {
    width: 870px;  
  }
`
const ImageCaption = styled(ReactMarkdown)`
  display: block;
  padding: 10px 20px 0;
  text-transform: uppercase;
  margin-bottom: 25px;
  font-size: 12px;
  font-weight: 500;
  color: ${vars.colors.key};
  ${vars.media.large} {
    padding: 0 0 0 190px;
  }
  & a {
    color: inherit;  
    &:hover {
      color: ${vars.colors.primary}
    }
  }
`
const Source = styled.div`
  text-transform: uppercase;
  font-size: 13px;
  font-weight: 500;
  margin: 25px 0;
  &:not(:last-child) {
    margin-bottom: 0;
  }
  & a {
    color: inherit;  
    &:hover {
      color: ${vars.colors.primary}
    }
  }
`
const SourceWrapper = styled.div`
  width: 100%;
  padding: 0 40px;
  ${vars.media.small} {
    padding: 0 50px;
  }
  ${vars.media.large} {
    width: 680px;
    padding: 0 0 0 190px;
  }
`

const Articles = styled.div`
  display: none; 
  ${vars.media.medium} {
    width: 225px;
    height: 100%;
    display: inline-block;
    flex-shrink: 0;
    padding: 30px 0 10px; 
    position: relative;
    z-index: 100;
    background: #fff;
    overflow-y: scroll;
    overflow-x: hidden;
  }
  ${vars.media.large} {
    width: 280px;
  }
  
  &::-webkit-scrollbar {
      width: 10px;
  }
  &::-webkit-scrollbar-track {
    background-color: #eee;
  }
  &::-webkit-scrollbar-thumb {
    background: #d4d4d4; 
    border-radius: 10px;
  }  
`
const SharingWrapper = styled.div`
  width: 100%;
  margin-top: 60px;
  padding: 0 40px;
  ${vars.media.small} {
    padding: 0 50px;
  }
  ${vars.media.large} {
    width: 680px;
    padding: 0 0 0 190px;
  }
`

@observer
export default class ArticleFull extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      path: '',
      year: this.props.location.pathname.slice(1, 5),
      id: this.props.location.pathname.slice(6),
    }
  }

  componentDidMount() {
    window.scroll(0, 0)
    stores.mainStore.getArticle(this.state.year, this.state.id)
    stores.mainStore.getArticlesList(this.state.year)
    stores.mainStore.setCurrentYear(this.state.year)
    this.setState({
      path: document.URL,
    })
  }

  render() {
    const store = stores.mainStore
    const {path, year} = this.state
    return (
      <Self>
        <Meta
          title={`${year} : WOWHAUS | МОСКВА 08 - 18`}
          description={store.article.lead}
        />
        <Articles>
          {store.articlesList.map((item, key) => {
            return (
              <ArticlePreview
                key={key}
                to={`/${year}/${item.id}`}
                type={item.type}
                month={item.month}
                title={item.previewTitle}
                author={item.authorName}
                authorPhoto={item.authorPhoto}
                onClick={() => stores.mainStore.closePopup()}
              />
            )
          })}
        </Articles>
        <Inner>
          <Close to={`/${year}`}>
            <CloseCrossIcon/>
          </Close>
          <Media style={store.article.imgCaption ? {marginBottom: '15px'} : null}>
            <LeadWrapper>
              <Lead>
                <LeadInner>
                  {store.article.type === 'speech' ?
                    <React.Fragment>Прямая речь</React.Fragment>
                    : store.article.lead
                  }
                </LeadInner>
              </Lead>
            </LeadWrapper>
            {store.article.imgSrc ? (
              <ImageWrapper>
                <Image
                  src={store.article.imgSrc}
                  src2x={store.article.imgSrc2x}
                />
              </ImageWrapper>
            ) : (
              null
            )}
            <TitleWrapper>
              <Title>{store.article.mainTitle}</Title>
            </TitleWrapper>
          </Media>
          {store.article.imgCaption ? (
            <ImageCaption escapeHtml={false} linkTarget='_blank'>{store.article.imgCaption}</ImageCaption>
          ) : (
            null
          )}
          {store.article.authorName && store.article.authorStatus ? (
            store.article.type === 'event' ? (
              <AuthorWrapper>
                <EventAuthor>
                  <EventAuthorPhoto src={store.article.authorPhoto}/>
                  <EventAuthorInfo>
                    <EventAuthorStatus>{store.article.authorStatus}</EventAuthorStatus>
                    <EventAuthorName>{store.article.authorName}:</EventAuthorName>
                  </EventAuthorInfo>
                </EventAuthor>
              </AuthorWrapper>
            ) : (
              null
            )
          ) : (
            null
          )}
          {store.speakers ? (
            store.speakers.map((speaker, key) => {
              return (
                <React.Fragment key={key}>
                  <AuthorWrapper>
                    <EventAuthor>
                      <EventAuthorPhoto src={speaker.photo}/>
                      <EventAuthorInfo>
                        <EventAuthorStatus>{speaker.status}</EventAuthorStatus>
                        <EventAuthorName>{speaker.name}:</EventAuthorName>
                      </EventAuthorInfo>
                    </EventAuthor>
                  </AuthorWrapper>
                  <Content>
                    <MarkdownContainer>
                      <ReactMarkdown
                        source={speaker.text}
                        escapeHtml={false}
                        linkTarget='_blank'
                      />
                    </MarkdownContainer>
                    {speaker.sourceName && speaker.sourceLink ? (
                      <Source>
                        <a target="_blank"
                           href={speaker.sourceLink}>{speaker.sourceName}</a>
                      </Source>
                    ) : speaker.sourceName ? (
                      <Source>{speaker.sourceName}</Source>
                    ) : (
                      null
                    )}
                  </Content>
                </React.Fragment>
              )
            })
          ) : (
            <Content>
              <MarkdownContainer>
                <ReactMarkdown
                  source={store.article.text}
                  escapeHtml={false}
                  linkTarget='_blank'
                />
              </MarkdownContainer>
            </Content>

          )}
          {store.article.sourceLink && store.article.sourceName ? (
            <SourceWrapper>
              <Source>
                <a target="_blank"
                   href={store.article.sourceLink}>{store.article.sourceName}</a>
              </Source>
            </SourceWrapper>
          ) : store.article.sourceName ? (
            <SourceWrapper>
              <Source>
                {store.article.sourceName}
              </Source>
            </SourceWrapper>
          ) : (
            null
          )}
          {store.article.type === 'speech' ? (
            <SpeechAuthor>{store.article.authorName}&nbsp;&mdash;&nbsp;{store.article.authorStatus}</SpeechAuthor>
          ) : (
            null
          )}
          <SharingWrapper>
            <Sharing url={path}/>
          </SharingWrapper>
        </Inner>
      </Self>
    )
  }
}
