import React from 'react'
import styled from 'styled-components'
import {Link} from 'react-router-dom'

import {vars} from '../../styles/index'
import SpeechIcon from '../icons/SpeechIcon'

const Self = styled(Link)`
  display: block;
  position: relative;
  width: 100%;
  min-height: auto;
  color: inherit;
  text-decoration: none;
  padding: 0 40px;
  &:not(:last-child) {
    margin-bottom: 30px;
    ${vars.media.medium} {
      margin-bottom: 54px;
    }
  }
  &:hover {
    cursor: pointer;
  }
  ${vars.media.medium} {
    padding: 0 20px 0 0;  
  }
`
const AvatarWrapper = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
  width: 120px;
  height: 91px;
  padding-top: 4px;
  vertical-align: bottom;
  ${Self}:hover &:before {
    background-color: ${vars.colors.primary};
  }
  &:before {
    content: "";
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 100;
    mix-blend-mode: multiply;
  }
`
const Avatar = styled('img')`
  display: block;
  width: 100%;
  height: 100%;
  border: none;
  object-fit: cover;
  object-position: center;
  filter: grayscale(100%);
`

const IconWrapper = styled.div`
  display: block;
  width: 44px;
  height: 48px;
  flex-shrink: 0;
  & #path {
    ${Self}:hover & {
      fill: ${vars.colors.primary};
    }
  }
  
`

const Author = styled('span')`
  position: relative;
  display: inline-block;
  font-size: 12px;
  line-height: 2;
  font-weight: 500;
  text-transform: uppercase;
  word-wrap: break-word;
  ${Self}:hover & {
    background-color: ${vars.colors.primary};
  }
`

const SpeechTitle = styled('h6')`
  display: block;
  padding-right: 25px;
  font-size: 18px;
  line-height: 20px;
  font-weight: 500;
  ${vars.media.small} {
    font-size: 16px;  
  }
  ${vars.media.large} {
  font-size: 18px;
 }
`

const SpeechTitleInner = styled('span')`
  ${Self}:hover & {
    background-color: ${vars.colors.primary};
  }
`

const Month = styled('span')`
  display: inline-block;
  color: ${vars.colors.primary};
  text-transform: uppercase;
  font-size: 12px;
  font-weight: 700;
`

const EventTitle = styled('h6')`
 font-size: 27px;
 text-transform: uppercase;
 line-height: 1.2;
 letter-spacing: -1px;
 ${vars.media.small} {
  font-size: 20px;  
 }
 ${vars.media.large} {
  font-size: 27px;  
 }
`

const EventTitleInner = styled('span')`
  color: ${vars.colors.primary};
  ${Self}:hover & {
    color: ${vars.colors.key};
    background-color: ${vars.colors.primary};
  }
`

export default class ArticlePreview extends React.Component {
  render() {
    const {
      month,
      type,
      title,
      author,
      authorPhoto,
      onClick,
      to,
    } = this.props
    return (
      <Self onClick={onClick} to={to}>
        {type === 'speech' ? (
          <React.Fragment>
            <Author>{author}</Author>
            <AvatarWrapper>
              <Avatar src={authorPhoto}/>
              <IconWrapper>
                <SpeechIcon/>
              </IconWrapper>
            </AvatarWrapper>
            <SpeechTitle>
              <SpeechTitleInner>
                {title}
              </SpeechTitleInner>
            </SpeechTitle>
          </React.Fragment>
        ) : type === 'event' ? (
          <React.Fragment>
            <Month>{month}</Month>
            <EventTitle>
              <EventTitleInner>{title}</EventTitleInner>
            </EventTitle>
          </React.Fragment>
        ) : (
          null
        )}
      </Self>
    )
  }
}
