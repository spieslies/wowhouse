import React from 'react'
import ReactMapGL, {Marker, NavigationControl, Popup} from 'react-map-gl'
import {observer} from 'mobx-react'
import styled from 'styled-components'
import {withSize} from 'react-sizeme'

import {vars} from '../../styles'
import stores from '../../stores'
import PinIcon from '../icons/PinIcon'
import {Link} from 'react-router-dom'

const TOKEN = 'pk.eyJ1Ijoic3BpZXMtYW5kLWxpZXMiLCJhIjoiY2puMXBkbmd0MXlzOTNrcXYzaTBzYjdseCJ9.e9O8zCTY5SwGFqf1nKVXWQ'
const mapStyle = 'mapbox://styles/spies-and-lies/cjo6vep6l1vra2rn1014dpaow'

const Self = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  z-index: 150;
  top: 134px;
  display: none;
  &.active {
    display: block;
  }
  & > div {
    width: 100%!important;
    height: 100%!important;
  }
  ${vars.media.medium} {
    position: relative;
    top: 0;
    display: block;
  }
`
const Pin = styled(Marker)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 30px;
  height: 30px;
  border-radius: 100%;
  font-size: 10px;
  fill: ${vars.colors.primary};
  &:hover {
    cursor: pointer;
    fill: ${vars.colors.key}
  }
`
const PinContainer = styled(Link)`
  display: block;
`
const Control = styled(NavigationControl)`
  position: absolute;
  top: 40%;
  right: 25px;
  display: none;
  ${vars.media.medium} {
    display: block;
  }
`
const Tooltip = styled(Popup)`
  display: none!important;
  ${PinContainer}:hover & {
    display: block!important;
  }
`
const TooltipInner = styled.span`
  background-color: ${vars.colors.primary};
  color: ${vars.colors.key};
  text-transform: uppercase;
  font-size: 27px;
  line-height: 1.2;
  letter-spacing: -1px;
`

@observer
class MapContainer extends React.Component {

  state = {
    viewport: {
      latitude: 55.752733,
      longitude: 37.622810,
      zoom: 11,
    },
  }

  render() {
    const store = stores.mainStore
    const {width, height} = this.props.size
    return (
      <Self className={store.showMap ? 'active' : null}>
        <ReactMapGL
          width={width}
          height={height}
          {...this.state.viewport}
          onViewportChange={(viewport) => this.setState({viewport})}
          mapboxApiAccessToken={TOKEN}
          mapStyle={mapStyle}
        >
          <Control onViewportChange={(viewport) => this.setState({viewport})}/>
          {store.pins.map((item, key) =>
            <PinContainer
              key={key}
              to={`/${item.yearId}/${item.id}`}
            >
              <Tooltip
                latitude={item.lat}
                longitude={item.long}
                closeButton={false}
                closeOnClick={true}
                offsetLeft={-20}
                offsetTop={-30}
                anchor="bottom-left"
              >
                <TooltipInner>
                  {item.title}
                </TooltipInner>
              </Tooltip>

              <Pin
                latitude={item.lat}
                longitude={item.long}
                offsetLeft={-20}
                offsetTop={-30}
              >
                <PinIcon/>
              </Pin>
            </PinContainer>,
          )}
        </ReactMapGL>
      </Self>

    )
  }
}

export default withSize()(MapContainer)
