import React from 'react'
import styled from 'styled-components'
import Spinner from '../icons/Spinner'

const Self = styled.div`
  position: fixed;
  z-index: 150;
  top: 0;
  bottom: 0;
  display: block;
  background: #fff;
  width: 100%;
  height: 100%;
`
const Inner = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`
export default class Loading extends React.Component {
  render() {
    return(
      <Self>
        <Inner>
          <Spinner/>
        </Inner>
      </Self>
    )
  }
}
