import React from 'react'
import styled from 'styled-components'
import vars from '../../styles/vars'

const AContent = styled('div')`
  width: 100%;
  height: 100%;
  display: flex;
  flex: 1;
  justify-content: center;
  flex-direction: column;
  position: relative;
  ${vars.media.medium} {
    flex-direction: row;
    justify-content: flex-start;
  }
`

export default class Content extends React.Component {
  render() {
    const {children} = this.props
    return (
      <AContent>{children}</AContent>
    )
  }
}
