import Header from './Header'
import Mainer from './Mainer'
import Sidebar from './Sidebar'
import Content from './Content'
import Footer from './Footer'

export {
  Header,
  Mainer,
  Sidebar,
  Content,
  Footer,
}
