import React from 'react'
import {observer} from 'mobx-react'
import styled from 'styled-components'
import {NavLink} from 'react-router-dom'

import {routes} from '../../utils'
import {vars} from '../../styles'
import stores from '../../stores'

import ar from '../../assets/icons/arrow-right.svg'
import al from '../../assets/icons/arrow-left.svg'

const ASidebar = styled('aside')`
    position: fixed;
    top: 56px;
    width: 100%;
    padding: 0 40px;
    z-index: 1000;
    background-color: ${vars.colors.white};
    &:before {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      width: 40px;
      background-image: url(${al});
      background-color:#fff;
      background-position: 50% 40%;
      background-size: contain;
      background-repeat: no-repeat;
      ${vars.media.medium} {
        display: none;
      }
    }
    &:after {
      content: '';
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      width: 40px;
      background-image: url(${ar});
      background-color:#fff;  
      background-position: 50% 40%;
      background-size: contain;
      background-repeat: no-repeat;
      ${vars.media.medium} {
        display: none;
      }
    }
    @media (min-width: 522px) {
      top: 51px;
    }
    ${vars.media.medium} {
      position: relative;
      top: 0;
      flex-shrink: 0;
      flex-direction: column;
      justify-content: space-between;
      width: 100px;
      padding: 0;
    }
`

const Inner = styled.div`
  display: flex;
  overflow-y: scroll;
  ${vars.media.medium} {
      position: relative;
      height: 100%;
      top: 0;
      flex-shrink: 0;
      flex-direction: column;
      justify-content: space-between;
      width: 100px;
      padding: 54px 0 20px;
    }
`

const SideBarLink = styled(NavLink)`
  display: block;
  font-family: 'DINCond', 'Roboto Condensed', sans-serif;
  font-size: 32px;
  text-decoration: none;
  font-weight: 500;
  text-align: center;
  padding: 20px 0;
  -webkit-tap-highlight-color: transparent;
  color: #e3e3e3;
  &:not(:last-child) {
    margin-right: 15px;
    ${vars.media.medium} {
      margin: 0;
    }
  }
  ${vars.media.medium} {
    color: inherit;
    padding: 0;
    font-size: 24px;
  }
  &:hover {
    color: ${vars.colors.primary};
  }
  &.active {
    color: ${vars.colors.primary};
  },
`

@observer
export default class Sidebar extends React.Component {
  render() {
    const store = stores.mainStore
    return (
      <ASidebar>
        <Inner>
          {routes.slice(1).map((route, index) =>
            <SideBarLink
              key={index}
              to={route.path}
              exact={true}
              children={route.name}
              className={store.currentYear === route.name ? 'active' : null}
              onClick={() => {
                store.pins = []
              }}
            />,
          )}
        </Inner>
      </ASidebar>
    )
  }
}
