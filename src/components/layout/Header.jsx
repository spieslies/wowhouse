import React from 'react'
import styled from 'styled-components'

import {vars} from '../../styles'
import Logo from '../logo/Logo'
import {Link} from 'react-router-dom'

const AHeader = styled.header`
  position: fixed;
  width: 100%; 
  text-transform: uppercase;
  font-size: 22px;
  line-height: 1;
  color: ${vars.colors.key};
  background-color:${vars.colors.white};
  z-index: 1000;
  ${vars.media.medium} {
    position:relative;
    flex: 0 0 auto;
    font-size: 48px;
  }
  ${vars.media.large} {
    font-size: 61px;    
  }
`

const Inner = styled('div')`
  display: flex;
  align-items: center;
  position: relative;
  margin-left: 41px;
  padding-left: 50px;
  background-color: ${vars.colors.primary};
  overflow: hidden;
  ${vars.media.medium} {
    display: block;
    margin-left: 330px;
  }
  ${vars.media.large} {
    margin-left: 380px;
  }
`
const Heading = styled.div`
  position: relative;
  width: 50%; 
  bottom: -3px;
  ${vars.media.medium} {
    width: 600px;
    position: absolute;
    margin-bottom: 0;
    bottom: -6px;
    left: -6px;
  }
`

const City = styled('span')`
  display: inline-block;
  letter-spacing: -2px;
`

const Date = styled('span') `
  display: inline-block;
  color: ${vars.colors.key};
  ${vars.media.medium} {
    margin-left: 25px;
  }
  ${vars.media.large} {
    margin-left: 8px;
  }
  ${vars.media.xlarge} {
    margin-left: 35px;
  }
`

const Tagline = styled('span')`
  width: 50%;
  position: relative;
  font-size: 13px;
  display: inline-block;
  font-weight: 500;
  bottom: -3px;
  ${vars.media.medium} {
    bottom: -6px;
    margin-left: 400px;
    font-size: 16px;
    line-height: 18px;
  }
  ${vars.media.large} {   
    margin-left: 530px;
    font-size: 21px;
    line-height: 23px;
  }
  ${vars.media.xlarge} {   
    margin-left: 594px;
    font-size: 21px;
    line-height: 23px;
  }
`
const LogoLine = styled.div`
  display: flex;
  background-color: ${vars.colors.key};
`
const LogoWrapper = styled(Link)`
  position: relative;
  display: block;
  width: 85px;
  height: 12px;
  background-color: ${vars.colors.white};
  ${vars.media.medium} {
    width: 146px;
    height: 20px;
    margin-left: 30px;  
  }
  &:before {
    content: '';
    display: none;
    position: absolute;
    left: -146px;
    width: 100%;
    height: 20px;
    background-color: ${vars.colors.white};
    ${vars.media.medium} {
      display: block;
    }
  }
`

export default class Header extends React.Component {
  render() {
    return (
      <AHeader>
        <Inner>
          <Heading>
            <City>Москва&nbsp;</City>
            <Date>08 &mdash; 18</Date>
          </Heading>
          <Tagline>Эволюция общественных<br/>пространств</Tagline>
        </Inner>
        <LogoLine>
          <LogoWrapper to='/'>
            <Logo/>
          </LogoWrapper>
        </LogoLine>
      </AHeader>
    )
  }
}
