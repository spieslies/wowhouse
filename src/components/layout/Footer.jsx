import React from 'react'
import {observer} from 'mobx-react'
import styled from 'styled-components'
import {vars} from '../../styles'
import stores from '../../stores'

const AFooter = styled('footer')`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 0 0 auto;
  padding: 20px 40px;
  background-color: ${vars.colors.key};
  color: ${vars.colors.white};
  text-transform: uppercase;
  font-size: 11px;
  z-index: 100;
  ${vars.media.small} {
    padding: 20px 50px;
    flex-direction: row;
    justify-content: space-between;
  }
  ${vars.media.medium} {
    padding: 20px 100px;
  }
`

const FooterItem = styled('div')`
  width: 100%;
  margin-bottom: 10px;
  ${vars.media.small} {
    width: 25%;
  }
  ${vars.media.medium} {
    width: 25%;
    margin-bottom: 0;
  }
`

const FooterLink = styled('a')`
  color: inherit;
  text-decoration: none;
  &.active {
    color: ${vars.colors.primary};
  }
  &:hover {
    color: ${vars.colors.primary};
    cursor: pointer;
  }
`
@observer
export default class Footer extends React.Component {
  render() {
    const store = stores.mainStore
    return (
      <AFooter>
        <FooterItem>© wowhaus, 2018</FooterItem>
        <FooterItem>
          <FooterLink href='http://wowhaus.ru/' target='_blank'>wowhaus.ru</FooterLink>
        </FooterItem>
        <FooterItem>
          <FooterLink href='https://www.facebook.com/wowhaus.ru/' target='_blank'>facebook</FooterLink>{' '}
          <FooterLink href='#' >instagram</FooterLink>{' '}
          <FooterLink href='#' >vk</FooterLink>
        </FooterItem>
        <FooterLink className={store.showPopup ? 'active' : null} onClick={() => store.togglePopup()}>О проекте</FooterLink>
      </AFooter>
    )
  }
}
