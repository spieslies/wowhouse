import React from 'react'
import styled from 'styled-components'

import {vars} from '../../styles'

const height = vars.header.height + vars.footer.height

const AMainer = styled('main')`
    flex: 1 0 auto;
    display: flex;
    flex-direction: column;
    padding-top: 134px;
    ${vars.media.medium} {
      padding-top: 0;
      flex-direction: row;
      align-items: stretch;
      height: calc(100% - ${height}px);  
    }
`

export default class Mainer extends React.Component {
  render() {
    const {children} = this.props
    return (
      <AMainer>{children}</AMainer>
    )
  }
}
