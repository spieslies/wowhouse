import React from 'react'

const GoToEventsIcon = () => (
  <svg viewBox="0 0 164.1 231.9">
    <path fill='#1d1d1b' d="M53 156.2l-38.2-37.4L53.6 81l3.5 3.5L22 118.8l34.5 33.8zM108.7 155.7L75 143.2V83.1l33.8 12.5v60.1zm-28.7-16l23.8 8.8V99.1L80 90.3v49.4z"/>
    <path fill='#1d1d1b' d="M109 154.8l-1.7-4.7 27.4-10.1V90.3L109 99.7l-1.7-4.6 32.4-12v60.4z"/>
  </svg>


)

export default GoToEventsIcon
