import React from 'react'

const CloseCrossIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 31">
    <path d="M0 28.6L28.8 0 30.8 2 2.2 30.7z"/>
    <path d="M0 2L2 0l28.7 28.6-2.2 2.1z"/>
  </svg>

)

export default CloseCrossIcon
