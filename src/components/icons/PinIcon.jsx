import React from 'react'

const PinIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 49">
    <path d="M28 14c0-7.7-6.3-14-14-14S0 6.3 0 14c0 11 14 35 14 35s14-24 14-35z"/>
  </svg>

)

export default PinIcon
