import React from 'react'

const SpeechIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 48">
    <path id='path' fill="#fff" d="M7.7 6.3h31.2v23.3H7.7z"/>
    <path d="M11.1 9.8h8.7v8.7l-6.4 8.2 1.1-8.2h-3.4V9.8zm12 0h8.7v8.7l-6.4 8.2 1.1-8.2h-3.3V9.8h-.1zM0 0v33.8h11.1V48l13.8-14.2h18.9V0H0z"/>
  </svg>

)

export default SpeechIcon
