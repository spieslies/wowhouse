import React from 'react'

const Logo = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 131.3 18">
      <path
        d="M105.5 11.9V0h-8.9v11.9c0 2.7 1.8 4.5 4.4 4.5 2.7 0 4.5-1.8 4.5-4.5"/>
      <path
        d="M116.6 16.4c2.7 0 4.3-1.2 4.3-3.3 0-.9-.3-1.7-.9-2.2-.6-.5-1.1-.7-2.4-.8l-2.1-.3c-1.4-.2-2.6-.7-3.4-1.4-.9-.8-1.4-1.9-1.4-3.3 0-2.6 1.6-4.4 4.3-4.9h-7.6v12c0 3.1-2 5.3-4.9 5.9h12c-1.7-.3-3-1-4.3-2.2l1.3-1.3a7.6 7.6 0 0 0 5.1 1.8M64.1 9.8h8.7V18h-8.7zM64.1 0h8.7v8.1h-8.7zM94.6 12V0H86l6.7 18h6.8c-2.9-.6-4.9-2.9-4.9-6M74.8 0v18h2.8l6.8-18zM81.2 13.9L79.7 18h11l-1.5-4.1zM88.6 12.2l-3.4-9.5-3.4 9.5zM18.8 0l-2.9 11.3L12.6 0H9.9L6.6 11.3 3.7 0H0l4.9 18h3l3.4-10.9L14.6 18h3l4.9-18zM32.7 14c-.5.6-1.3.9-2.3.9-1 0-1.8-.4-2.3-.9-.7-.8-.9-1.6-.9-5s.2-4.2.9-4.9c.5-.6 1.3-.9 2.3-.9 1 0 1.8.4 2.3.9.7.7.9 1.5.9 4.9 0 3.4-.2 4.2-.9 5M30.4 0c-2.1 0-3.7.7-5 1.9-1.9 1.8-1.8 4-1.8 7s-.1 5.3 1.8 7c1.3 1.2 2.9 1.9 5 1.9s3.7-.7 4.9-1.9c1.9-1.8 1.8-4 1.8-7s0-5.2-1.8-7A6.63 6.63 0 0 0 30.4 0M55.6 0L53 11.3 49.9 0h-2.5l-3.1 11.3L41.7 0h-3.5l4.6 18h2.7l3.2-10.9L51.8 18h2.7l4.6-18z"/>
      <path
        d="M118.3 18h13V0h-12.8c1.4.3 2.5.8 3.6 1.8L120.8 3a5.8 5.8 0 0 0-4.3-1.6c-2.5 0-3.9 1.3-3.9 3.3 0 .9.3 1.6.8 2.1.6.5 1.5.8 2.5 1l2 .3c1.7.3 2.6.6 3.4 1.3 1 .8 1.5 2.1 1.5 3.6.1 2.6-1.7 4.4-4.5 5z"/>
    </svg>
  )
}

export default Logo
