import {observable, action} from 'mobx'
import axios from 'axios/index'

const host = 'http://85.119.144.140:9005/years'

export class MainStore {

  @observable
  articlesList = []

  @observable
  article = []

  @observable
  speakers = []

  @observable
  pins = []

  @observable
  item = []

  @observable
  loading = false

  @observable
  showPopup = false

  @observable
  showMap = false

  @observable
  currentYear = ''


  @action
  setCurrentYear = (year) => {
    this.currentYear = year
  }

  @action
  togglePopup() {
    this.showPopup = !this.showPopup
  }

  @action
  closePopup = () => {
    this.showPopup = false
  }

  @action
  toggleMap() {
    this.showMap = !this.showMap
  }

  @action
  closeMap() {
    this.showMap = true
  }

  @action
  getArticle = (location, id) => {
    this.article = []
    this.loading = true
    axios.get(`${host}/${location}/articles?id=${id}`)
      .then((response) => {
        this.loading = false
        this.article = response.data[0]
        this.speakers = response.data[0].speakers
      })
      .catch((error) => {
        this.loading = false
        console.log(error)
      })
  }

  @action
  getArticlesList = (location) => {
    this.articlesList = []
    this.loading = true
    axios.get(`${host}/${location}/articles`)
      .then((response) => {
        this.loading = false
        this.articlesList = response.data.sort((a, b) => a.position - b.position)
      })
      .catch((error) => {
        this.loading = false
        console.log(error)
      })
  }

  @action
  getPins = (location) => {
    this.loading = true
    axios.get(`${host}/${location}/pins`)
      .then((response) => {
        this.loading = false
        this.pins = response.data
      })
      .catch((error) => {
        this.loading = false
        console.log(error)
      })
  }
}

export default MainStore
