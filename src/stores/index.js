import MainStore from './MainStore'

const mainStore = new MainStore()

export {
  MainStore,
}

export default {
  mainStore,
}
