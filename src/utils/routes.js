import {
  Year2008,
  Year2009,
  Year2010,
  Year2011,
  Year2012,
  Year2013,
  Year2014,
  Year2015,
  Year2016,
  Year2017,
  Year2018,
} from '../pages'

const routes = [
  {
    name: '2018',
    path: '/2018',
    exact: true,
    component: Year2018,
  },
  {
    name: '2018',
    path: '/',
    exact: true,
    component: Year2018,
  },
  {
    name: '2017',
    path: '/2017',
    exact: false,
    component: Year2017,
  },
  {
    name: '2016',
    path: '/2016',
    exact: false,
    component: Year2016,
  },
  {
    name: '2015',
    path: '/2015',
    exact: false,
    component: Year2015,
  },
  {
    name: '2014',
    path: '/2014',
    exact: false,
    component: Year2014,
  },
  {
    name: '2013',
    path: '/2013',
    exact: false,
    component: Year2013,
  },
  {
    name: '2012',
    path: '/2012',
    exact: false,
    component: Year2012,
  },
  {
    name: '2011',
    path: '/2011',
    exact: false,
    component: Year2011,
  },
  {
    name: '2010',
    path: '/2010',
    exact: false,
    component: Year2010,
  },
  {
    name: '2009',
    path: '/2009',
    exact: false,
    component: Year2009,
  },
  {
    name: '2008',
    path: '/2008',
    exact: false,
    component: Year2008,
  },
]

export default routes
