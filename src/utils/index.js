import routes from './routes'
import chunkArray from './chunkArray'

export {
  routes,
  chunkArray
}
