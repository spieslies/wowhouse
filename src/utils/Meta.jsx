import React from 'react'
import {Helmet} from 'react-helmet'

export default class Meta extends React.Component {
  render() {
    const {title, description} = this.props
    return(
      <Helmet>
        <title>{title}</title>
        {description ? (
          <meta name="description" content={description}/>
        ) : (
          <meta name="description" content='Это все факты, цитаты, выдержки. Мы не претендуем на воссоздание объективной картины мира, это всего лишь субъективный взгляд на минувшее десятилетие и попытка осмысления того, как живет Город, через призму работы нашей, наших друзей и коллег.'/>
        )}
      </Helmet>
    )
  }
}
