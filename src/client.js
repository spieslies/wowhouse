import App from './App'
import BrowserRouter from 'react-router-dom/BrowserRouter'
import React from 'react'
import {hydrate} from 'react-dom'
import {global as GlobalStyles} from './styles'

hydrate(
  <React.Fragment>
    <GlobalStyles/>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </React.Fragment>,
  document.getElementById('root'),
)

if (module.hot) {
  module.hot.accept()
}
