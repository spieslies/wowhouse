import React from 'react'
import Route from 'react-router-dom/Route'
import Switch from 'react-router-dom/Switch'
import styled from 'styled-components'

import {Header, Mainer, Sidebar, Footer, Content} from './components'
import {vars} from './styles'
import {routes} from './utils'
import NotFound from './pages/NotFound'
import ArticleFull from './components/article/ArticleFull'
import MapContainer from './components/map/MapContainer'
import About from './pages/About'

const Self = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  font-family: 'DINPro', sans-serif;
  font-weight: 400;
  color: ${vars.colors.key};
`

const App = () => (
  <Self>
    <Header/>
    <Mainer>
      <Sidebar/>
      <Content>
        <Switch>
          {routes.map((route, index) =>
            <Route
              key={index}
              path={route.path}
              exact={true}
              component={route.component}
            />,
          )}
          <Route path={`/:yearID/:articleId/`} component={props => <ArticleFull {...props}/>}/>
          <Route path="*" component={NotFound}/>

        </Switch>
        <About/>
        <MapContainer/>
      </Content>
    </Mainer>
    <Footer/>
  </Self>

)

export default App
