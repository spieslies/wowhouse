import App from './App'
import React from 'react'
import express from 'express'
import {useStaticRendering} from 'mobx-react'
import {StaticRouter} from 'react-router-dom'
import {renderToString} from 'react-dom/server'
import {ServerStyleSheet} from 'styled-components'

import {global as GlobalStyles} from './styles'
import {Helmet} from 'react-helmet'
const assets = require(process.env.RAZZLE_ASSETS_MANIFEST)
const server = express()
useStaticRendering(true)
server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', (req, res) => {
    const context = {}
    const sheet = new ServerStyleSheet()
    const markup = renderToString(
      sheet.collectStyles(
        <React.Fragment>
          <GlobalStyles/>
          <StaticRouter context={context} location={req.url}>
            <App/>
          </StaticRouter>
        </React.Fragment>
        ,
      ),
    )
    const styleTags = sheet.getStyleTags()
    const helmet = Helmet.renderStatic()

    if (context.url) {
      res.redirect(context.url)
    } else {
      res.status(200).send(
        `<!doctype html>
    <html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${
          assets.client.css
            ? `<link rel="stylesheet" href="${assets.client.css}">`
            : ''
          }
        ${
          process.env.NODE_ENV === 'production'
            ? `<script src="${assets.client.js}" defer></script>`
            : `<script src="${assets.client.js}" defer crossorigin></script>`
          }
        ${styleTags}
    </head>
    <body>
        <div id="root">${markup}</div>
    </body>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130567708-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-130567708-1');
    </script>
</html>`,
      )
    }
  })

export default server
